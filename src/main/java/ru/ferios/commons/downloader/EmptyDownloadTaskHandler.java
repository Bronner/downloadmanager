package ru.ferios.commons.downloader;

import ru.ferios.commons.downloader.exception.DownloadException;
import ru.ferios.commons.downloader.exception.RedownloadException;

/**
 * Пустой обработчик событий для задачи загрузки
 * Может быть использован для расширения другими обработчиками
 */
public class EmptyDownloadTaskHandler implements DownloadTaskHandler {

    @Override
    public void onTaskStart(DownloadTask task) {
    }

    @Override
    public void onTaskComplete(DownloadTask task) throws RedownloadException {
    }

    @Override
    public void onTaskAbort(DownloadTask task) {
    }

    @Override
    public void onTaskError(DownloadTask task, DownloadException exception) {
    }
}
