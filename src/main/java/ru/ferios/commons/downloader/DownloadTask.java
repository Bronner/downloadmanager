package ru.ferios.commons.downloader;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.commons.downloader.exception.DownloadException;
import ru.ferios.commons.downloader.exception.RedownloadException;

import java.io.File;

@Slf4j
public class DownloadTask {

    @Getter
    private final File outputFile;

    @Getter
    private final DownloadUrl repo;

    private final DownloadTaskHandler handler;

    @Getter
    @Setter
    private long expectedSize;

    public DownloadTask(File outputFile, DownloadUrl repo, DownloadTaskHandler handler) {
        this.outputFile = outputFile;
        this.repo = repo;
        this.handler = handler;
    }

    public DownloadTask(File outputFile, DownloadUrl repo) {
        this(outputFile, repo, new EmptyDownloadTaskHandler());
    }

    protected void onStart() {
        handler.onTaskStart(this);
    }

    protected void onComplete() throws RedownloadException {
        handler.onTaskComplete(this);
    }

    protected void onAbort() {
        handler.onTaskAbort(this);
    }

    protected void onError(DownloadException exception) {
        handler.onTaskError(this, exception);
    }

    @Override
    public String toString() {
        return "DownloadTask{" +
                "outputFile=" + outputFile +
                ", repo=" + repo +
                '}';
    }
}
