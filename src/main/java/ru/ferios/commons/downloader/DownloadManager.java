package ru.ferios.commons.downloader;

import com.github.kevinsawicki.http.HttpRequest;
import com.github.kevinsawicki.http.HttpRequest.HttpRequestException;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkState;

@Slf4j
public class DownloadManager {

    private static final int PROGRESS_UPDATE_INTERVAL = 200;

    private static final int DOWNLOAD_THREADS = 4;

    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    @Getter
    private final Queue<DownloadTask> downloadQueue;

    @Getter
    private final Set<DownloadTask> failures;

    @Getter
    private final Set<DownloadTask> successful;

    private final Stopwatch timer;

    private final AtomicInteger numActiveDownloads = new AtomicInteger();

    private final Set<DownloaderListener> listeners;

    private final DownloadWorker worker;

    private volatile State state = State.READY;

    @Getter
    private DownloadProgress progress = new DownloadProgress();

    public DownloadManager() {
        //executorService = new ExceptionalThreadPoolExecutor(DOWNLOAD_THREADS, DOWNLOAD_THREADS, 30L, TimeUnit
        // .SECONDS);
        downloadQueue = new ConcurrentLinkedQueue<>();
        failures = Sets.newConcurrentHashSet();
        successful = Sets.newConcurrentHashSet();
        timer = Stopwatch.createUnstarted();
        listeners = Sets.newConcurrentHashSet();
        worker = new DownloadWorker(this, downloadQueue);
    }

    /**
     * Запускает загрузку файлов в очереди
     * Файлы с ошибками при прошлой загрузке так же будут добавлены в текущую очередь загрузок
     */
    public void start() {
        executorService.execute(this::startDownload);
    }

    /**
     * Останавливает загрузку
     */
    public void stop() {
        state = State.STOPPED;
        if (timer.isRunning()) {
            timer.stop();
        }
        onDownloadAbort();
        executorService.shutdown();
    }

    /**
     * Добавляет загрузки в очередь
     *
     * @param tasks Загрузки
     */
    public void addDownload(Collection<DownloadTask> tasks) {
        checkReady();
        downloadQueue.addAll(tasks);
    }

    /**
     * Добавляет загрузки в очередь
     *
     * @param tasks Загрузки
     */
    public void addDownload(DownloadTask... tasks) {
        checkReady();
        for (DownloadTask d : tasks) {
            downloadQueue.add(d);
        }
    }

    /**
     * Добавляет объект слушателя
     *
     * @param listener Слушатель
     */
    public void addLisneter(DownloaderListener listener) {
        listeners.add(listener);
    }

    /**
     * Удаляет объект слушателя
     *
     * @param listener Слушатель
     */
    public void removeListener(DownloaderListener listener) {
        listeners.remove(listener);
    }

    public State getState() {
        return state;
    }

    private void startDownload() {
        checkReady();
        failures.clear();
        successful.clear();
        if (downloadQueue.isEmpty()) {
            log.info("Очередь загрузок пуста");
            onDownloadDone();
        } else {
            try {
                fetchTotalDownloadSize();
            } catch (HttpRequestException e) {
                log.error("Невозможно подготовить файл к загрузке", e);
                onDownloadError();
                return;
            }
            onDownloadStart();

            int realNumThreads = downloadQueue.size() < DOWNLOAD_THREADS ? downloadQueue.size() : DOWNLOAD_THREADS;
            log.info("Начата загрузка: {} файлов / {} потоков", downloadQueue.size(), realNumThreads);
            executorService.submit(new ProgressUpdater());
            for (int i = 0; i < realNumThreads; i++) {
                executorService.submit(worker);
            }
        }
    }

    private void fetchTotalDownloadSize() throws HttpRequestException {
        onDownloadPrepare();
        progress.setTotal(0);
        for (DownloadTask download : downloadQueue) {
            log.info("prepare {}", download);
            if (download.getExpectedSize() == 0L) {
                HttpRequest request = HttpRequest.head(download.getRepo().getUrl());
                download.setExpectedSize(request.contentLength());
            }
            progress.incrementTotal(download.getExpectedSize());
        }
    }

    void onDownloadTaskComplete(DownloadTask task) {
        for (DownloaderListener l : listeners) {
            l.onDownloadTaskComplete(task);
        }

        successful.add(task);

        if (downloadQueue.isEmpty()) {
            onDownloadDone();
        }
    }

    void onDownloadTaskError(DownloadTask task) {
        failures.add(task);

        if (downloadQueue.isEmpty()) {
            onDownloadError();
        }
    }

    private void onDownloadDone() {
        state = State.READY;
        updateProgress();
        log.info("Все загрузки завершены за " + timer.toString());

        for (DownloaderListener l : listeners) {
            l.onDownloadComplete(this);
        }

        stop();
    }

    private void onDownloadError() {
        state = State.FAILED;

        for (DownloaderListener l : listeners) {
            l.onDownloadError(this);
        }

        stop();
    }

    private void onDownloadStart() {
        state = State.DOWNLOADING;

        for (DownloaderListener l : listeners) {
            l.onDownloadStart(this);
        }
    }

    private void updateProgress() {
        progress.setRemainingFiles(downloadQueue.size() + numActiveDownloads.get());
        long currentDownloadSize = 0;
        long currentSpeed = 0;

        //        for (DownloadTask file : downloadQueue) {
        //            currentDownloadSize += file.getCurrentSize();
        //            currentSpeed += file.getDataRate();
        //        }

        //        progress.setCurrent(currentDownloadSize);
        progress.setCurrent(worker.getBytesReaded());
        progress.setCurrentSpeed(currentSpeed);

        for (DownloaderListener listener : listeners) {
            listener.onProgressChanged(progress);
        }
    }

    private void onDownloadPrepare() {
        state = State.PREPARING;
        timer.reset();
        timer.start();

        for (DownloaderListener listener : listeners) {
            listener.onDownloadPrepare(this);
        }
    }

    private void onDownloadAbort() {
        for (DownloaderListener listener : listeners) {
            listener.onDownloadAbort(this);
        }
    }

    /**
     * Проверяет была ли начата загрузка
     *
     * @throws IllegalStateException
     */
    private void checkReady() throws IllegalStateException {
        checkState(getState().equals(State.READY), "Загрузка уже начата");
    }

    public enum State {

        /**
         * Готов к запуску
         */
        READY,

        /**
         * Выполнение подготовки к загрузке
         */
        PREPARING,

        /**
         * Выполняется загрузка
         */
        DOWNLOADING,

        /**
         * Загрузка была остановлена
         */
        STOPPED,

        /**
         * Некоторые загурзки завершены с ошибками
         */
        FAILED
    }

    private class ProgressUpdater implements Runnable {

        @Override
        public void run() {
            while (state.equals(State.PREPARING) || state.equals(State.DOWNLOADING)) {
                updateProgress();
                try {
                    Thread.sleep(PROGRESS_UPDATE_INTERVAL);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}
