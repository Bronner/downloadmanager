package ru.ferios.commons.downloader;

/**
 * Интерфейс, который должны реализовывать и зарегистрировать
 * с помощью {@link DownloadManager#addLisneter(DownloaderListener)} все его слушатели.
 */
public interface DownloaderListener {

    /**
     * Событие происходит при начале загрузки
     *
     * @param downloadManager Загрузчик
     */
    void onDownloadStart(DownloadManager downloadManager);

    /**
     * Событие происходит при обновлении прогресса
     *
     * @param progress Прогресс
     */
    void onProgressChanged(DownloadProgress progress);

    /**
     * Событие происходит при завершении текущей загрузки
     *
     * @param task Задача
     */
    void onDownloadTaskComplete(DownloadTask task);

    /**
     * Событие происходит при завершении всех загрузок
     *
     * @param downloadManager Загрузчик
     */
    void onDownloadComplete(DownloadManager downloadManager);

    /**
     * Событие происходит при наличии ошибок загрузки
     *
     * @param downloadManager Загрузчик
     */
    void onDownloadError(DownloadManager downloadManager);

    /**
     * Событие происходит при подготовке к загрузке
     *
     * @param downloadManager Загрузчик
     */
    void onDownloadPrepare(DownloadManager downloadManager);

    /**
     * Событие происходит при отмене загрузок
     *
     * @param downloadManager Загрузчик
     */
    void onDownloadAbort(DownloadManager downloadManager);
}
