package ru.ferios.commons.downloader;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;

import java.util.Queue;

/**
 * Общий прогресс загрузки
 */
public class DownloadProgress {

    /**
     * Общий размер загрузок в байтах
     */
    private long total;

    /**
     * Текущее количество загруженных байтов
     */
    private long current;

    /**
     * Список изменений скорости для подсчета среднего значения
     */
    private Queue<Long> speed = Queues.synchronizedQueue(EvictingQueue.create(64));

    /**
     * Оставшееся количество файлов в очереди
     */
    private int remainingFiles;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public Long getAverageSpeed() {
        Long averageSpeed = 0L;
        for (Long val : speed) {
            averageSpeed += val;
        }
        return averageSpeed / speed.size();
    }

    public Long getCurrentSpeed() {
        final Long peek = speed.peek();
        return peek == null ? 0 : peek;
    }

    public void setCurrentSpeed(Long speed) {
        this.speed.add(speed);
    }

    public int getRemainingFiles() {
        return remainingFiles;
    }

    public void setRemainingFiles(int remainingFiles) {
        this.remainingFiles = remainingFiles;
    }

    public void incrementTotal(long size) {
        setTotal(getTotal() + size);
    }

    public void incrementCurrent(long size) {
        setCurrent(getCurrent() + size);
    }
}
