package ru.ferios.commons.downloader.exception;

import javax.annotation.Nullable;

public class DownloadException extends Exception {

    public DownloadException() {
        super();
    }

    public DownloadException(String message) {
        super(message);
    }

    public DownloadException(String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    public DownloadException(Throwable cause) {
        super(cause);
    }
}
