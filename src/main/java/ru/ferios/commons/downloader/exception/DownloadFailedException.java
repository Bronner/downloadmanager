package ru.ferios.commons.downloader.exception;

import javax.annotation.Nullable;

public class DownloadFailedException extends DownloadException {

    public DownloadFailedException() {
        super();
    }

    public DownloadFailedException(String message) {
        super(message);
    }

    public DownloadFailedException(String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    public DownloadFailedException(Throwable cause) {
        super(cause);
    }
}
