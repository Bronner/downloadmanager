package ru.ferios.commons.downloader.exception;

import javax.annotation.Nullable;

public class RedownloadException extends DownloadException {

    public RedownloadException() {
        super();
    }

    public RedownloadException(String message) {
        super(message);
    }

    public RedownloadException(String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    public RedownloadException(Throwable cause) {
        super(cause);
    }
}
