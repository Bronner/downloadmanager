package ru.ferios.commons.downloader;

import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;

public class DownloadUrl {

    private final Deque<String> mirrors = new LinkedList<>();

    public DownloadUrl(String path, String[] mirrors) {
        for (String mirror : mirrors) {
            this.mirrors.add(mirror + path);
        }
    }

    public DownloadUrl(String... exactUrls) {
        Collections.addAll(mirrors, exactUrls);
    }

    public String getUrl() {
        return mirrors.getFirst();
    }

    public int getMirrorsCount() {
        return mirrors.size();
    }

    public void nextUrl() {
        String oldUrl = mirrors.poll();
        mirrors.addLast(oldUrl);
    }

    @Override
    public String toString() {
        return getUrl();
    }
}
