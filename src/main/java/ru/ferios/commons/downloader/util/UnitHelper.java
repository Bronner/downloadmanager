package ru.ferios.commons.downloader.util;

public final class UnitHelper {

    private UnitHelper() {
    }

    public static String toHumanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) {
            return bytes + " Б";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "кМГТПЭ" : "КМГТПЭ").charAt(exp - 1) + (si ? "" : "и");
        return String.format("%.1f %sБ", bytes / Math.pow(unit, exp), pre);
    }

    public static double getAverage(double[] d) {
        double a = 0;
        int k = 0;

        for (double curd : d) {
            if (curd == 0) {
                continue;
            }
            a += curd;
            ++k;
        }

        if (k == 0) {
            return 0;
        }
        return a / k;
    }
}
