package ru.ferios.commons.downloader;

import com.github.kevinsawicki.http.HttpRequest;
import com.github.kevinsawicki.http.HttpRequest.HttpRequestException;
import com.google.common.io.CountingInputStream;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.commons.downloader.DownloadManager.State;
import ru.ferios.commons.downloader.exception.DownloadAbortedException;
import ru.ferios.commons.downloader.exception.DownloadFailedException;
import ru.ferios.commons.downloader.exception.RedownloadException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Queue;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
class DownloadWorker implements Runnable {

    /**
     * Стандартный размер буфера
     */
    private static final int BUF_SIZE = 8192;

    private final DownloadManager manager;

    private final Queue<DownloadTask> downloadQueue;

    private final DownloadProgress progress;

    /**
     * Максимальное количество повторных попыток загрузки для одного файла
     * <p>
     * Сдандартное значение: 3
     */
    private final int maxAttempts;

    @Getter
    private volatile long bytesReaded;

    DownloadWorker(DownloadManager manager, Queue<DownloadTask> downloadQueue, int maxAttempts) {
        checkNotNull(manager, "manager");
        checkNotNull(downloadQueue, "downloadQueue");
        this.manager = manager;
        this.progress = manager.getProgress();
        this.downloadQueue = downloadQueue;
        this.maxAttempts = maxAttempts;
    }

    DownloadWorker(DownloadManager manager, Queue<DownloadTask> downloadQueue) {
        this(manager, downloadQueue, 3);
    }

    private static void deleteFileSilently(File file) {
        if (file == null) {
            return;
        }
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException io) {
            log.error("Невозможно удалить файл", io);
        }
    }

    private static void ensureFileWritable(File target) {
        if ((target.getParentFile() != null) && (!target.getParentFile().isDirectory())) {
            log.info("Создание папки " + target.getParentFile());

            if ((!target.getParentFile().mkdirs()) && (!target.getParentFile().isDirectory())) {
                throw new RuntimeException("Невозможно создать папку " + target.getParentFile());
            }
        }

        if ((target.isFile()) && (!target.canWrite())) {
            throw new RuntimeException("Недостаточно прав для записи в " + target);
        }
    }

    @Override
    public void run() {
        DownloadTask task = null;
        try {
            while ((task = downloadQueue.poll()) != null) {
                int attempt = 0;
                while (attempt < maxAttempts) {
                    attempt++;
                    log.info("Загрузка {} (попытка {}/{})", task.getRepo().getUrl(), attempt, maxAttempts);
                    try {
                        download(task);
                        task.onComplete();
                        manager.onDownloadTaskComplete(task);
                        log.info("Загрузка завершена {}", task.getOutputFile());
                        break; // Done
                    } catch (RedownloadException e) {
                        log.info("Загрузка не завершена: {} ({})", e.getMessage(), task.getRepo().getUrl());
                    } catch (DownloadFailedException e) {
                        if (attempt >= maxAttempts) {
                            deleteFileSilently(task.getOutputFile());
                            task.onError(e);
                            manager.onDownloadTaskError(task);
                            log.error("Превышено число попыток для загрузки {}", task.getRepo().getUrl());
                        }
                        log.warn("Невозможно загрузить {} ({})", task.getRepo().getUrl(), e.getCause());
                    }
                }
            }
        } catch (DownloadAbortedException e) {
            log.info("Загрузка отменена");
            deleteFileSilently(task.getOutputFile());
            Iterator<DownloadTask> it = downloadQueue.iterator();
            while (it.hasNext()) {
                it.next().onAbort();
                it.remove();
            }
        }
    }

    private void download(DownloadTask task) throws DownloadAbortedException, DownloadFailedException {
        ensureFileWritable(task.getOutputFile());
        int attempt = 0;
        int mirrorsCount = task.getRepo().getMirrorsCount();
        Throwable lastException = null;
        HttpRequest request = null;
        // Пытаемся загрузить файл пока есть доступные зеркала
        while (attempt < mirrorsCount) {
            attempt++;
            checkNotStopped();
            try {
                request = HttpRequest.get(task.getRepo().getUrl()).useCaches(false).trustAllCerts();
                if (request.ok()) {
                    final long startTime = System.nanoTime();
                    long currentSize = 0;
                    double elapsedTime;
                    try (CountingInputStream inputStream = new CountingInputStream(request.stream());
                            FileOutputStream outputStream = new FileOutputStream(task.getOutputFile())) {
                        byte[] buf = new byte[BUF_SIZE];
                        while (true) {
                            checkNotStopped();
                            int r = inputStream.read(buf);
                            if (r == -1) {
                                break;
                            }
                            outputStream.write(buf, 0, r);
                            bytesReaded += inputStream.getCount() - currentSize;
                            currentSize = inputStream.getCount();
                            elapsedTime = (System.nanoTime() - startTime) / 1000000D;
                            progress.setCurrentSpeed((long) (currentSize / (elapsedTime / 1000D)));
                        }
                        //Успешно завершаем метод
                        return;
                    }
                } else {
                    throw new IOException(request.code() + " - " + request.message());
                }
            } catch (HttpRequestException | IOException e) {
                task.getRepo().nextUrl();
                lastException = e;
            } finally {
                if (request != null) {
                    request.disconnect();
                }
            }
        }
        // Файл не был загружен
        throw new DownloadFailedException(task.getOutputFile().getPath(), lastException);
    }

    private void checkNotStopped() throws DownloadAbortedException {
        if (manager.getState().equals(State.STOPPED)) {
            throw new DownloadAbortedException();
        }
    }
}
