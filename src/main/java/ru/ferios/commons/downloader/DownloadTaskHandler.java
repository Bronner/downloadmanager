package ru.ferios.commons.downloader;

import ru.ferios.commons.downloader.exception.DownloadException;
import ru.ferios.commons.downloader.exception.RedownloadException;

/**
 * Обработчик событий определенной задачи загрузки
 */
public interface DownloadTaskHandler {

    void onTaskStart(DownloadTask task);

    void onTaskComplete(DownloadTask task) throws RedownloadException;

    void onTaskAbort(DownloadTask task);

    void onTaskError(DownloadTask task, DownloadException exception);
}
