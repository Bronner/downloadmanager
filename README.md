Пример простого использования

```
DownloadManager dm = new DownloadManager();
dm.addDownload(new DownloadTask(new File("file1.zip"), new DownloadUrl("http://localhost/file1.zip")));
dm.addDownload(new DownloadTask(new File("file2.zip"), new DownloadUrl("http://localhost/file2.zip")));
dm.start();
```
